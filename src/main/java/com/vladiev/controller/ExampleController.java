package com.vladiev.controller;

import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Random;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
@Controller
@RequestMapping("/")
public class ExampleController {
    @Autowired
    private Random random;
    private Gson gson = new Gson();


    @RequestMapping(value = "/")
    @ResponseBody
    public String root(Model model) {
        return "Hello world";
    }


    @RequestMapping(value = "/index")
    public String index(Model model) {
        return "index";
    }


    @RequestMapping(value = "/get_random_table")
    public String getRandomTable(Model model) {
        return "random_table";
    }


    @RequestMapping(value = "/echo")
    @ResponseBody
    public String echo(Model model, @RequestParam String world) {
        return world;
    }

    @RequestMapping(value = "/echo_page")
    public String echoPage(Model model, @RequestParam String world) {
        model.addAttribute("world", world);
        return "echo_page";
    }

    @RequestMapping(value = "/get_random_number",
            produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getRandomNumber(Model model) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("randomNumber", random.nextInt());
        return jsonObject.toString();
    }
}
