package com.vladiev;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.bio.SocketConnector;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.File;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class ServerRunner {
    public static void main(String[] args) {
        Server jettyServer = null;
        try {
            System.out.println(new File(".").getAbsolutePath());
            jettyServer = new Server();

            SocketConnector conn = new SocketConnector();
            conn.setPort(8086);
            jettyServer.setConnectors(new Connector[]{conn});

            WebAppContext context = new WebAppContext();

            context.setContextPath("/");
            context.setWar("src/main/webapp");

            jettyServer.setHandler(context);
            jettyServer.start();
            System.out.println("Server started!");
        } catch (Throwable ignore) {
            if (jettyServer != null) {
                try {
                    jettyServer.stop();
                } catch (Throwable e1) {
                    e1.printStackTrace();
                }
            }
            ignore.printStackTrace();
        }
    }
}
