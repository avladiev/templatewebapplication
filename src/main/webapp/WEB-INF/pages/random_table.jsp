<%@ page import="java.util.Random" %>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    final Random random = new Random();
    final int maxNumber = 10;
    final int numberRows = random.nextInt(maxNumber);
    final int numberColumns = random.nextInt(maxNumber);
%>

<html>

<head>
    <title>random Table</title>
</head>

<body>
<table border="2">
    <caption> Table</caption>
    <thead>
    <tr>
        <%for (int i = 0; i < numberColumns; ++i) {%>
        <th><%=i%>
        </th>
        <%}%>
    </tr>
    </thead>
    <tbody>

    <%for (int i = 0; i < numberRows; ++i) {%>

    <tr>
        <%for (int j = 0; j < numberColumns; ++j) {%>
        <td>(<%=i%>,<%=j%>)</td>
        <%}%>
    </tr>


    <%}%>

    </tbody>

</table>
</body>

</html>